# MEAN Stack coding interview
This challenge consists in the use of two codes: MEAN-frontend-coding-interview and MEAN-backend-coding-interview.
Those work together and show basic information of pizza orders.

### Principal libraries and frameworks being used
- Angular 12 (Javascript framework)
- ExpressJS (NodeJS framework)
- Mongoose (Mongo DB ORM)

The code also include a few libraries that don't affect in the logic so they are excluded from the required experience: dotenv (.env files in NodeJS), morgan (request logs in ExpressJS) and Tailwindcss (CSS tool)

## Before the coding interview
- Pull both frontend and backend code and install all dependencies
- spin up a MongoDB instance, this can be local or remote.
- Once the MongoDB instance is up, insert the mocks in the database
- Check file file `.env` in the backend code and update the values in case you need to use a different port or mongodb url
- You will need a REST client like Postman or Insonmia

