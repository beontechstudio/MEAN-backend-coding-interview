const  { config } = require('dotenv');
config();

const express = require("express");
const app = express();
const logger = require("morgan");

const { connectDB } = require('./db');
const { setPizzaRoutes } = require("./routes/pizzas.routes.js");
const { setOrdersRoutes } = require("./routes/orders.routes.js");

app.use(logger('dev'));
app.use(express.json());

setPizzaRoutes(app, "/pizzas");
setOrdersRoutes(app, "/orders");

connectDB()
  .then(() => {
    app.listen(process.env.NODE_PORT, (error) => {
      if (error) {
        throw error;
      }

      console.log("Server started");
    });
  })
  .catch(error => {
    throw error;
  });


