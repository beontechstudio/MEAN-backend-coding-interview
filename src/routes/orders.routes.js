const express = require("express");
const { Order } = require('../models/order.model');

exports.setOrdersRoutes = (app, basePath) => {
  const router = express.Router();

  router.get('/', async (request, response) => {
    const orders = await Order.find().lean().exec();

    return response.send({ orders: orders });
  });

  app.use(basePath, router);
};
