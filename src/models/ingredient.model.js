const mongoose = require('mongoose');

const IngredientSchema = new mongoose.Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true },
  isAvailable: { type: Boolean, required: true },
  inventory: { type: Number, required: true },
}, { timestamps: true, collection: 'ingredients' });

exports.Ingredient = mongoose.model('Ingredient', IngredientSchema);
