const mongoose = require('mongoose');

const OrderPizzaItem = new mongoose.Schema({
  pizzaId: { type: mongoose.Types.ObjectId, required: true },
  extraIngredients: {
    type: [new mongoose.Schema({
      ingredientId: { type: mongoose.Types.ObjectId, required: true },
      name: { type: String, required: true },
      price: { type: String, required: true },
    })],
    required: true
  },
  basePrice: { type: Number, required: true },
  extraPrice: { type: Number, required: true },
  endPrice: { type: Number, required: true }
}, { _id: false, timestamps: false });

const OrderScheme = new mongoose.Schema({
  status: { type: String, required: true },
  pizzas: { type: [OrderPizzaItem], required: true },
  subTotal: { type: Number, required: true },
  total: { type: Number, required: true },
}, { timestamps: true, collection: 'orders' });

exports.Order = mongoose.model('Order', OrderScheme);
